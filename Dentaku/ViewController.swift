
import UIKit

class ViewController: UIViewController{
    
    @IBOutlet weak var result: UILabel! //攻めラベル
    @IBOutlet weak var calculat: UILabel!   //受けラベル
    @IBOutlet weak var sign: UILabel!   //記号ラベル
    @IBOutlet weak var paste_field: UITextField! //貼り付け用テキストフィールド
    var result_num: Int = 0
    var result_flg = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        result.text = ""
        calculat.text = ""
    }
    
    @IBAction func numPushed(sender: AnyObject) {
        // 「Error」が出ていたら表示をリセット
        if result.text == "0" || result_flg {
            result.text = ""
            result_flg = false
        }
        result.text! += String(sender.tag)
    }
    
    @IBAction func signPushed(sender: AnyObject) {
        // 「Error」が出ていたら表示をリセット
        if result.text == "Error" {
            result.text = ""
        }
        
        // 攻めと受けのラベルに値が入っている時に計算してから記号を表示
        if calculat.text != "" && result.text != "" {
            if sign.text == "+" {
                result_num = Int(self.calculat.text!)! + Int(result.text!)!
                calculat.text = String(result_num)
            } else if sign.text == "-" {
                result_num = Int(self.calculat.text!)! - Int(result.text!)!
                calculat.text = result_num.description
            } else if sign.text == "×" {
                result_num = Int(self.calculat.text!)! * Int(result.text!)!
                calculat.text = result_num.description
            } else if sign.text == "÷" {
                // 0除算ならば「Error」を出す
                if result.text == "0" {
                    result.text = "Error"
                    calculat.text = ""
                    sign.text = ""
                    result_flg = true
                    return
                } else {
                    result_num = Int(calculat.text!)! / Int(result.text!)!
                    calculat.text = result_num.description
                }
            }
        } else {
            calculat.text = result.text
        }
        if sender.tag == 0 {
            sign.text = "+"
        } else if sender.tag == 1 {
            sign.text = "-"
        } else if sender.tag == 2 {
            sign.text = "×"
        } else if sender.tag == 3 {
            sign.text = "÷"
        }
        self.result.text = ""
    }
    
    @IBAction func minus(sender: AnyObject) {
        if result.text != "" {
            result_num = Int(self.result.text!)! * -1
            result.text = result_num.description
        }
    }
    
    @IBAction func equalPushed(sender: AnyObject) {
        result_flg = true
        if sign.text == "+" {
            result_num = Int(calculat.text!)! + Int(result.text!)!
            result.text = String(self.result_num)
        } else if sign.text == "-" {
            result_num = Int(calculat.text!)! - Int(result.text!)!
            result.text = result_num.description
        } else if sign.text == "×" {
            result_num = Int(calculat.text!)! * Int(result.text!)!
            result.text = result_num.description
        } else if sign.text == "÷" {
            // 0除算ならば「Error」を出す
            if result.text == "0" {
                result.text = "Error"
            } else {
                result_num = Int(calculat.text!)! / Int(result.text!)!
                result.text = result_num.description
            }
        }
        
        // 値が入っていない時に「=」ボタンを押されたら何もしない
        if result.text != "" {
            calculat.text = ""
            sign.text = ""
        }
    }
    
    // 結果のコピー
    @IBAction func copyResult(sender: AnyObject) {
        if result_flg{
            let board = UIPasteboard.generalPasteboard()
            board.setValue(result.text!, forPasteboardType: "public.text")
        }
    }
    
    // 貼り付け用テキストフィールドにどの記号が入っているか検索
    // 記号をキーに文字列を分割　配列に入れる
    // 配列を使用して計算する
    // テキストフィールドに記号が入っていない場合はテキストフィールドが初期化されるだけ
    @IBAction func pasteCul(sender: AnyObject) {
        if paste_field.text != "" {
            result_flg = true
            var arr: [String] = []
            result.text = ""    // 記号が入っていない場合のため初期化
            let predicate = NSPredicate(format: "SELF MATCHES '\\\\d+'")
            if (paste_field.text!.rangeOfString("+") != nil) {
                arr = paste_field.text!.componentsSeparatedByString("+")
                // 文字列が数字のみかどうかの判定
                if predicate.evaluateWithObject(arr[0]) && predicate.evaluateWithObject(arr[1]) {
                    result_num = Int(arr[0])! + Int(arr[1])!
                    result.text = String(self.result_num)
                }
            } else if (paste_field.text!.rangeOfString("-") != nil) {
                arr = paste_field.text!.componentsSeparatedByString("-")
                if predicate.evaluateWithObject(arr[0]) && predicate.evaluateWithObject(arr[1]) {
                    result_num = Int(arr[0])! - Int(arr[1])!
                    result.text = result_num.description
                }
            } else if (paste_field.text!.rangeOfString("×") != nil) {
                arr = paste_field.text!.componentsSeparatedByString("×")
                if predicate.evaluateWithObject(arr[0]) && predicate.evaluateWithObject(arr[1]) {
                    result_num = Int(arr[0])! * Int(arr[1])!
                    result.text = result_num.description
                }
            } else if (paste_field.text!.rangeOfString("÷") != nil) {
                // 0除算ならば「Error」を出す
                arr = paste_field.text!.componentsSeparatedByString("÷")
                if arr[1] == "0" {
                    result.text = "Error"
                } else {
                    if predicate.evaluateWithObject(arr[0]) && predicate.evaluateWithObject(arr[1]) {
                        result_num = Int(arr[0])! / Int(arr[1])!
                        result.text = result_num.description
                    }
                }
            }
            paste_field.resignFirstResponder()
            paste_field.text = ""
        }
    }
    
    @IBAction func clearPushed(sender: AnyObject) {
        result.text = ""
        result_flg = false
    }
    
    @IBAction func allClearPushed(sender: AnyObject) {
        result.text = ""
        calculat.text = ""
        sign.text = ""
        result_flg = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

